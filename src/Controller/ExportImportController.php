<?php

namespace Drupal\senapi_export_import\Controller;



use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\system\FileDownloadController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileSystem;


class ExportImportController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The file download controller.
   *
   * @var \Drupal\system\FileDownloadController
   */
  protected $fileDownloadController;

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      new FileDownloadController(),
      $container->get('file_system')
    );
  }

  /**
   * ExportImportController constructor.
   *
   * @param \Drupal\system\FileDownloadController $file_download_controller
   */
  public function __construct(FileDownloadController $file_download_controller, FileSystem $file_system) {
    $this->fileDownloadController = $file_download_controller;
    $this->fileSystem = $file_system;
  }

  /**
   * Descarga tarball de archivos media exportado.
   */
  public function downloadExport() {

    return [
      '#type' => 'markup',
      '#markup' => 'Prueba ruta'
    ];
  }
}